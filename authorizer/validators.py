from datetime import timedelta
from dateutil import parser

from store import store


ACCOUNT_ALREADY_INITIALIZED = 'account-already-initialized'
INSUFFICIENT_LIMIT = 'insufficient-limit'
CARD_NOT_ACTIVE = 'card-not-active'
HIGH_FRECUENCY_SMALL_INTERVAL = 'high-frequency-small-interval'
DOUBLED_TRANSACTION = 'doubled-transaction'
NO_VALID_OPERATION = 'no-valid-operation'
ACCOUNT_NOT_INITIALIZED = 'account-not-initialized'

VALID_OPERATIONS = ['account', 'transaction']


def account_exist_create(operation=None):
    account = store.get_account()
    violations = []
    if account:
        violations = [ACCOUNT_ALREADY_INITIALIZED]
    return violations


def account_exist_transaction(operation=None):
    account = store.get_account()
    violations = []
    if not account:
        violations = [ACCOUNT_NOT_INITIALIZED]
    return violations


def enough_limit(operation):
    account = store.get_account()
    violations = []
    if operation['amount'] > account['availableLimit']:
        violations = [INSUFFICIENT_LIMIT]
    return violations


def card_active(operation=None):
    account = store.get_account()
    violations = []
    if not account['activeCard']:
        violations = [CARD_NOT_ACTIVE]
    return violations


def high_frequency(operation,  delta_seconds=120, limit_operations=3):
    transactions = store.get_transactions()
    time = parser.parse(operation['time'])
    delta = timedelta(seconds=delta_seconds)
    count = 1
    for transaction in transactions:
        if time - transaction['time'] > delta:
            break
        else:
            count += 1
    violations = []
    if count > limit_operations:
        violations = [HIGH_FRECUENCY_SMALL_INTERVAL]
    return violations


def double_transaction(operation, delta_seconds=120):
    transactions = store.get_transactions()
    time = parser.parse(operation['time'])
    amount = operation['amount']
    merchant = operation['merchant']
    delta = timedelta(seconds=delta_seconds)
    violations = []
    for transaction in transactions:
        if time - transaction['time'] > delta:
            break
        if transaction['merchant'] == merchant and transaction['amount'] == amount:
            violations = [DOUBLED_TRANSACTION]
            break
    return violations


def valid_operation(operation_obj):
    violations = [NO_VALID_OPERATION]
    for operation in VALID_OPERATIONS:
        if operation in operation_obj:
            violations = []
            break
    return violations