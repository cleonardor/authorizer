import json

from brokers import dispacher

def std_client():
    """
    Using the standard input and output to read the operation and write the result.
    Read and write in json format
    """
    raw_operation = input()
    operation = json.loads(raw_operation)
    response = dispacher(operation)
    print(json.dumps(response))