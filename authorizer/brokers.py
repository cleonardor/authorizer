import json

from store import store
from validators import (
    account_exist_create,
    account_exist_transaction,
    enough_limit,
    card_active,
    high_frequency,
    double_transaction,
    valid_operation,
)

def dispacher(operation_obj):
    if "account" in operation_obj:
        response = create_account_process(operation_obj)
    elif "transaction" in operation_obj:
        response = transaction_process(operation_obj)
    else:
        response = no_known_operation_process(operation_obj)
    return response


def no_known_operation_process(operation_obj):
    # The operation could be an unknown but suported operation (it means that the dispacher don't 
    # know it but exists) or could be a not valid operation
    validators = [valid_operation]
    violations = apply_validations(operation_obj, validators)
    response = store.get_account_obj()
    response["violations"] = violations
    return response


def create_account_process(operation_obj):
    operation = operation_obj['account']
    validators = [account_exist_create]
    violations = apply_validations(operation, validators)
    if not violations:
        store.set_account(operation)
    response = store.get_account_obj()
    response["violations"] = violations
    return response


def transaction_process(operation_obj):
    operation = operation_obj['transaction']
    validators = [
        account_exist_transaction,
        enough_limit,
        card_active,
        high_frequency,
        double_transaction
    ]
    violations = apply_validations(operation, validators)
    if not violations:
        store.apply_transaction(operation)
    response = store.get_account_obj()
    response["violations"] = violations
    return response


# --- Utils ---
def apply_validations(operation, validators):
    violations = []
    for validator in validators:
        violations += validator(operation)
        if violations:
            break
    return violations