#!/usr/bin/python3

from clients import std_client

def main():
    while True:
        try:
            std_client()
        except EOFError:
            break

if __name__ == '__main__':
    main()
