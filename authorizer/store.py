import threading
from collections import deque
from dateutil import parser

class Store:
    lock = threading.Lock()
    account = {}
    transactions = deque()

    def set_account(self, operation):
        self.account = operation

    def get_account_obj(self):
        return {'account': self.account}

    def get_account(self):
        return self.account

    def _add_transaction(self, operation):
        transaction = dict(operation)
        transaction['time'] = parser.parse(transaction['time'])
        self.transactions.appendleft(transaction)

    def get_transactions_obj(self):
        return [
            {
                'transaction': {
                    'merchant': transaction['merchant'],
                    'amount': transaction['amount'], 
                    'time': transaction['time'].isoformat(timespec='milliseconds').replace('+00:00', 'Z')
                }
            } for transaction in self.transactions
        ]

    def get_transactions(self):
        return list(self.transactions)

    def apply_transaction(self, operation):
        with self.lock:
            self._add_transaction(operation)
            self.account['availableLimit'] -= operation['amount']

    def _clean(self):
        self.account = {}
        self.transactions = deque()

store = Store()