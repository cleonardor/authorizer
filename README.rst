Code Observations
-----------------

- The code is structured in modules as following:
    - **validators**: It contains all functions that validate the request operations.
    - **brokers**: It contains the functions that process every operation and also the dispacher function that know what is the propper function for every operation type.
    - **clients**: It contains the clients that can receive or process request operations. Currently there is a client that read and write for standard input but it could contain  a rest API client, or a queue broker client, etc.
    - **store**: It contains the structure that holds the data, it could be an simple class as currently it is or could be something more complex like a database.
- The tests are structured in modules, one for every code module. They contain the unit test for every defined function and some integration tests.
- Every validator fuction should receive at least one argument for the *operation* that will be validated, even if it is unused and should return a list of violations if they exists or a empty list if not. With that we can apply validators in a generic way.
- For every operation a serie of validators could be apply, this secuence is stoped when some validator fails because doesn't make sense continue spending time validating somethig that alredy is invalid.
- When an operation has an effect in the data store involving multiple update, all the related operations must be proccesed in an atomic way. It is reflected in the use of a lock in the store module. Is true that it only have sense when we are using threads (which is no our current case) but we need to take it into account if the clients or data store change.



Deployment
----------

The following details show how to deploy and run this application.

Docker run
^^^^^^^^^^

Go to the project root folder. Note: The docker build process could take some time, please be patient.

    $ docker build -t authorizer_image .

    $ docker run -it --name authorizer -v $PWD:/app authorizer_image /bin/bash


Program and tests run
^^^^^^^^^^^^^^^^^^^^^

Once inside the container you can run the **authorize** program and **tests**

- run the program to read input from stdin

    $ autorize

- run the program with input in a file (operations). That file could be whatever file that exist in the program project folder because that is mounted as a volume in the docker container.

    $ authorize < operations

- run all tests with module output results

    $ pytest

- run all tests with test output results

    $ pytest -v