FROM ubuntu:18.04
RUN apt-get -yqq update; apt-get -yqq install python3 python3-virtualenv
ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m virtualenv --python=/usr/bin/python3 $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
COPY requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install -r requirements.txt
RUN echo 'alias authorize="python /app/authorizer/authorize.py"' >> ~/.bashrc