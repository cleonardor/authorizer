from collections import deque

from authorizer import (
    brokers,
    validators,
    store,
)


def test_known_operation_without_account():
    operation_obj = {
        "transaction": {"merchant": "Store A", "amount": 30, "time": "2019-02-13T10:01:30.000Z"}
    }
    response = brokers.no_known_operation_process(operation_obj)
    expected = {"account": {}, "violations": []}
    assert response == expected


def test_known_operation_with_account(monkeypatch):
    account = {"activeCard": True, "availableLimit": 100}
    monkeypatch.setattr(brokers.store, 'account', account)
    def mock_validator(operation):
        return []
    monkeypatch.setattr(brokers, 'valid_operation', mock_validator)
    operation_obj = {
        "transaction": {"merchant": "Store A", "amount": 30, "time": "2019-02-13T10:01:30.000Z"}
    }
    response = brokers.no_known_operation_process(operation_obj)
    expected = {"account": account, "violations": []}
    assert response == expected

def test_no_known_operation_without_account():
    operation_obj = {
        "credit": {"merchant": "Store A", "amount": 30, "time": "2019-02-13T10:01:30.000Z"}
    }
    response = brokers.no_known_operation_process(operation_obj)
    expected = {"account": {}, "violations": [validators.NO_VALID_OPERATION]}
    assert response == expected


def test_no_known_operation_with_account(monkeypatch):
    account = {"activeCard": True, "availableLimit": 100}
    monkeypatch.setattr(brokers.store, 'account', account)
    violations = [validators.NO_VALID_OPERATION]
    def mock_validator(operation):
        return violations
    monkeypatch.setattr(brokers, 'valid_operation', mock_validator)
    operation_obj = {
        "credit": {"merchant": "Store A", "amount": 30, "time": "2019-02-13T10:01:30.000Z"}
    }
    response = brokers.no_known_operation_process(operation_obj)
    expected = {"account": account, "violations": violations}
    assert response == expected


def test_create_acccount_new(monkeypatch):
    monkeypatch.setattr(brokers.store, 'account', {})
    def mock_validator(operation):
        return []
    monkeypatch.setattr(brokers, 'account_exist_create', mock_validator)
    operation_obj = {"account": {"activeCard": True, "availableLimit": 100}}
    response = brokers.create_account_process(operation_obj)
    expected = operation_obj
    expected["violations"] = []
    assert response == expected


def test_create_account_already_exist(monkeypatch):
    account = {"activeCard": True, "availableLimit": 100}
    monkeypatch.setattr(brokers.store, 'account', account)
    violations = [validators.ACCOUNT_ALREADY_INITIALIZED]
    def mock_validator(operation):
        return violations
    monkeypatch.setattr(brokers, 'account_exist_create', mock_validator)
    operation_obj = {"account": {"activeCard": True, "availableLimit": 200}}
    response = brokers.create_account_process(operation_obj)
    expected = {"account": account, 'violations': violations}
    assert response == expected


def test_transaction_no_valid_all_validator_fails(monkeypatch):
    account = {"activeCard": True, "availableLimit": 150}
    monkeypatch.setattr(brokers.store, 'account', account)
    violations = ["VIOLATIONS"]
    def mock_validator(*arg, **kwargs):
        return violations
    monkeypatch.setattr(brokers, 'account_exist_transaction', mock_validator)
    monkeypatch.setattr(brokers, 'enough_limit', mock_validator)
    monkeypatch.setattr(brokers, 'card_active', mock_validator)
    monkeypatch.setattr(brokers, 'high_frequency', mock_validator)
    monkeypatch.setattr(brokers, 'double_transaction', mock_validator)
    operation_obj = {
        "transaction": {"merchant": "Store A", "amount": 30, "time": "2019-02-13T10:01:30.000Z"}
    }
    response = brokers.transaction_process(operation_obj)
    expected = {"account": account, "violations": violations}
    assert response == expected


def test_transaction_no_valid_one_validators_fail(monkeypatch):
    account = {"activeCard": True, "availableLimit": 200}
    monkeypatch.setattr(brokers.store, 'account', account)
    violations = ["VIOLATIONS"]
    def mock_validator_fail(*arg, **kwargs):
        return violations
    def mock_validator_ok(*arg, **kwargs):
        return []
    monkeypatch.setattr(brokers, 'account_exist_transaction', mock_validator_ok)
    monkeypatch.setattr(brokers, 'enough_limit', mock_validator_ok)
    monkeypatch.setattr(brokers, 'card_active', mock_validator_ok)
    monkeypatch.setattr(brokers, 'high_frequency', mock_validator_fail)
    monkeypatch.setattr(brokers, 'double_transaction', mock_validator_ok)
    operation_obj = {
        "transaction": {"merchant": "Store B", "amount": 30, "time": "2019-02-13T10:01:30.000Z"}
    }
    response = brokers.transaction_process(operation_obj)
    expected = {"account": account, "violations": violations}
    assert response == expected


def test_transaction_valid(monkeypatch):
    monkeypatch.setattr(brokers.store, 'account', {"activeCard": True, "availableLimit": 250})
    monkeypatch.setattr(brokers.store, 'transactions', deque())
    def mock_validator(*arg, **kwargs):
        return []
    monkeypatch.setattr(brokers, 'account_exist_transaction', mock_validator)
    monkeypatch.setattr(brokers, 'enough_limit', mock_validator)
    monkeypatch.setattr(brokers, 'card_active', mock_validator)
    monkeypatch.setattr(brokers, 'high_frequency', mock_validator)
    monkeypatch.setattr(brokers, 'double_transaction', mock_validator)
    operation_obj = {
        "transaction": {"merchant": "Store C", "amount": 30, "time": "2019-02-13T10:01:30.000Z"}
    }
    response = brokers.transaction_process(operation_obj)
    expected = {"account": {"activeCard": True, "availableLimit": 220}, 'violations': []}
    assert response == expected


def test_transaction_no_valid_not_account(monkeypatch):
    monkeypatch.setattr(brokers.store, 'account', {})
    violations = [validators.ACCOUNT_NOT_INITIALIZED]
    def mock_validator_fail(*arg, **kwargs):
        return violations
    def mock_validator_ok(*arg, **kwargs):
        return []
    monkeypatch.setattr(brokers, 'account_exist_transaction', mock_validator_fail)
    monkeypatch.setattr(brokers, 'enough_limit', mock_validator_ok)
    monkeypatch.setattr(brokers, 'card_active', mock_validator_ok)
    monkeypatch.setattr(brokers, 'high_frequency', mock_validator_ok)
    monkeypatch.setattr(brokers, 'double_transaction', mock_validator_ok)
    operation_obj = {
        "transaction": {"merchant": "Store C", "amount": 30, "time": "2019-02-13T10:01:30.000Z"}
    }
    response = brokers.transaction_process(operation_obj)
    expected = {"account": {}, "violations": violations}
    assert response == expected


def test_integration_dispacher():
    brokers.store._clean()
    operations = [
        {"account": {"activeCard": True, "availableLimit": 250}},
        {"transaction": {"merchant": "Store C", "amount": 30, "time": "2019-02-13T10:01:30.000Z"}},
        {"account": {"activeCard": True, "availableLimit": 300}},
        {"transaction": {"merchant": "Store C", "amount": 30, "time": "2019-02-13T10:02:30.000Z"}},
        {"transaction": {"merchant": "Store S", "amount": 50, "time": "2019-02-13T10:04:30.000Z"}},
        {"transaction": {"merchant": "Store A", "amount": 40, "time": "2019-02-13T10:04:50.000Z"}},
        {"transaction": {"merchant": "Store B", "amount": 70, "time": "2019-02-13T10:05:30.000Z"}},
        {"transaction": {"merchant": "Store X", "amount": 50, "time": "2019-02-13T10:05:57.000Z"}},
        {"transaction": {"merchant": "Store H", "amount": 10, "time": "2019-02-13T10:08:00.000Z"}},
    ]
    expecteds = [
        {"account": {"activeCard": True, "availableLimit": 250}, "violations": []},
        {"account": {"activeCard": True, "availableLimit": 220}, "violations": []},
        {"account": {"activeCard": True, "availableLimit": 220}, "violations": [validators.ACCOUNT_ALREADY_INITIALIZED]},
        {"account": {"activeCard": True, "availableLimit": 220}, "violations": [validators.DOUBLED_TRANSACTION]},
        {"account": {"activeCard": True, "availableLimit": 170}, "violations": []},
        {"account": {"activeCard": True, "availableLimit": 130}, "violations": []},
        {"account": {"activeCard": True, "availableLimit": 60}, "violations": []},
        {"account": {"activeCard": True, "availableLimit": 60}, "violations": [validators.HIGH_FRECUENCY_SMALL_INTERVAL]},
        {"account": {"activeCard": True, "availableLimit": 50}, "violations": []},
    ]
    for (operation, expected) in zip(operations, expecteds):
        response = brokers.dispacher(operation)
        assert response == expected


def test_integration_dispacher2():
    brokers.store._clean()
    operations = [
        {"account": {"activeCard": False, "availableLimit": 250}},
        {"transaction": {"merchant": "Store C", "amount": 30, "time": "2019-02-13T10:01:30.000Z"}},
        {"account": {"activeCard": True, "availableLimit": 300}},
        {"transaction": {"merchant": "Store C", "amount": 30, "time": "2019-02-13T10:02:30.000Z"}},
        {"transaction": {"merchant": "Store S", "amount": 50, "time": "2019-02-13T10:04:30.000Z"}},
        {"transaction": {"merchant": "Store A", "amount": 40, "time": "2019-02-13T10:04:50.000Z"}},
        {"transaction": {"merchant": "Store B", "amount": 70, "time": "2019-02-13T10:05:30.000Z"}},
        {"transaction": {"merchant": "Store X", "amount": 50, "time": "2019-02-13T10:05:57.000Z"}},
        {"transaction": {"merchant": "Store H", "amount": 10, "time": "2019-02-13T10:08:00.000Z"}},
    ]
    expecteds = [
        {"account": {"activeCard": False, "availableLimit": 250}, "violations": []},
        {"account": {"activeCard": False, "availableLimit": 250}, "violations": [validators.CARD_NOT_ACTIVE]},
        {"account": {"activeCard": False, "availableLimit": 250}, "violations": [validators.ACCOUNT_ALREADY_INITIALIZED]},
        {"account": {"activeCard": False, "availableLimit": 250}, "violations": [validators.CARD_NOT_ACTIVE]},
        {"account": {"activeCard": False, "availableLimit": 250}, "violations": [validators.CARD_NOT_ACTIVE]},
        {"account": {"activeCard": False, "availableLimit": 250}, "violations": [validators.CARD_NOT_ACTIVE]},
        {"account": {"activeCard": False, "availableLimit": 250}, "violations": [validators.CARD_NOT_ACTIVE]},
        {"account": {"activeCard": False, "availableLimit": 250}, "violations": [validators.CARD_NOT_ACTIVE]},
        {"account": {"activeCard": False, "availableLimit": 250}, "violations": [validators.CARD_NOT_ACTIVE]},
    ]
    for (operation, expected) in zip(operations, expecteds):
        response = brokers.dispacher(operation)
        assert response == expected