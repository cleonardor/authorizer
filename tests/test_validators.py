from dateutil import parser
from collections import deque

from authorizer import validators


def test_no_exist_account_create_account(monkeypatch):
    monkeypatch.setattr(validators.store, 'account', {})
    assert validators.account_exist_create() == []


def test_exist_account_create_account(monkeypatch):
    monkeypatch.setattr(validators.store, 'account', {"activeCard": True, "availableLimit": 100})
    assert validators.account_exist_create() == [validators.ACCOUNT_ALREADY_INITIALIZED]


def test_sufficient_limit(monkeypatch):
    monkeypatch.setattr(validators.store, 'account', {"activeCard": True, "availableLimit": 100})
    operation = {"merchant": "Store", "amount": 20, "time": "2019-02-13T10:00:00.000Z" }
    assert validators.enough_limit(operation) == []


def test_no_sufficient_limit(monkeypatch):
    monkeypatch.setattr(validators.store, 'account', {"activeCard": True, "availableLimit": 100})
    operation = {"merchant": "Store", "amount": 120, "time": "2019-02-13T10:00:00.000Z"}
    assert validators.enough_limit(operation) == [validators.INSUFFICIENT_LIMIT]


def test_equal_limit(monkeypatch):
    monkeypatch.setattr(validators.store, 'account', {"activeCard": True, "availableLimit": 100})
    operation = {"merchant": "Store", "amount": 100, "time": "2019-02-13T10:00:00.000Z" }
    assert validators.enough_limit(operation) == []


def test_card_active(monkeypatch):
    monkeypatch.setattr(validators.store, 'account', {"activeCard": True, "availableLimit": 100})
    assert validators.card_active() == []


def test_card_no_active(monkeypatch):
    monkeypatch.setattr(validators.store, 'account', {"activeCard": False, "availableLimit": 100})
    assert validators.card_active() == [validators.CARD_NOT_ACTIVE]


def test_no_high_frequency_small_interval(monkeypatch):
    monkeypatch.setattr(validators.store, 'transactions', deque([
        {"merchant": "Store", "amount": 20, "time": parser.parse("2019-02-13T10:01:00.000Z")},
        {"merchant": "Store", "amount": 20, "time": parser.parse("2019-02-13T10:00:00.000Z")},
    ]))
    operation = {"merchant": "Store", "amount": 20, "time": "2019-02-13T10:01:30.000Z"}
    assert validators.high_frequency(operation) == []


def test_high_frequency_small_interval(monkeypatch):
    monkeypatch.setattr(validators.store, 'transactions', deque([
        {"merchant": "Store", "amount": 20, "time": parser.parse("2019-02-13T10:01:30.000Z")},
        {"merchant": "Store", "amount": 20, "time": parser.parse("2019-02-13T10:01:00.000Z")},
        {"merchant": "Store", "amount": 20, "time": parser.parse("2019-02-13T10:00:00.000Z")},
    ]))
    operation = {"merchant": "Store", "amount": 20, "time": "2019-02-13T10:01:50.000Z"}
    violations = validators.high_frequency(operation, delta_seconds=120, limit_operations=3)
    assert  violations == [validators.HIGH_FRECUENCY_SMALL_INTERVAL]



def test_high_frequency_small_interval_limit(monkeypatch):
    monkeypatch.setattr(validators.store, 'transactions', deque([
        {"merchant": "Store", "amount": 20, "time": parser.parse("2019-02-13T10:01:30.000Z")},
        {"merchant": "Store", "amount": 20, "time": parser.parse("2019-02-13T10:01:00.000Z")},
        {"merchant": "Store", "amount": 20, "time": parser.parse("2019-02-13T10:00:00.000Z")},
    ]))
    operation = {"merchant": "Store", "amount": 20, "time": "2019-02-13T10:02:00.000Z"}
    violations = validators.high_frequency(operation, delta_seconds=120, limit_operations=3)
    assert  violations == [validators.HIGH_FRECUENCY_SMALL_INTERVAL]


def test_no_high_frequency_long_interval(monkeypatch):
    monkeypatch.setattr(validators.store, 'transactions', deque([
        {"merchant": "Store", "amount": 20, "time": parser.parse("2019-02-13T10:03:00.000Z")},
        {"merchant": "Store", "amount": 20, "time": parser.parse("2019-02-13T10:02:00.000Z")},
        {"merchant": "Store", "amount": 20, "time": parser.parse("2019-02-13T10:01:00.000Z")},
    ]))
    operation = {"merchant": "Store", "amount": 20, "time": "2019-02-13T10:05:00.000Z"}
    violations = validators.high_frequency(operation, delta_seconds=120, limit_operations=3)
    assert violations == []


def test_high_frequency_long_interval(monkeypatch):
    monkeypatch.setattr(validators.store, 'transactions', deque([
        {"merchant": "Store", "amount": 20, "time": parser.parse("2019-02-13T10:03:30.000Z")},
        {"merchant": "Store", "amount": 20, "time": parser.parse("2019-02-13T10:02:35.000Z")},
        {"merchant": "Store", "amount": 20, "time": parser.parse("2019-02-13T10:02:30.000Z")},
        {"merchant": "Store", "amount": 20, "time": parser.parse("2019-02-13T10:01:00.000Z")},
        {"merchant": "Store", "amount": 20, "time": parser.parse("2019-02-13T10:00:00.000Z")},
    ]))
    operation = {"merchant": "Store", "amount": 20, "time": "2019-02-13T10:03:45.000Z"}
    violations = validators.high_frequency(operation, delta_seconds=120, limit_operations=3)
    assert violations == [validators.HIGH_FRECUENCY_SMALL_INTERVAL]


def test_no_double_transaction_different_stores_in_interval(monkeypatch):
    monkeypatch.setattr(validators.store, 'transactions', deque([
        {"merchant": "Store A", "amount": 20, "time": parser.parse("2019-02-13T10:01:00.000Z")},
        {"merchant": "Store B", "amount": 20, "time": parser.parse("2019-02-13T10:00:00.000Z")},
    ]))
    operation = {"merchant": "Store C", "amount": 20, "time": "2019-02-13T10:01:30.000Z"}
    assert validators.double_transaction(operation, delta_seconds=120) == []


def test_no_double_transaction_same_store_out_interval(monkeypatch):
    monkeypatch.setattr(validators.store, 'transactions', deque([
        {"merchant": "Store A", "amount": 20, "time": parser.parse("2019-02-13T10:01:00.000Z")},
        {"merchant": "Store B", "amount": 20, "time": parser.parse("2019-02-13T10:00:00.000Z")},
    ]))
    operation = {"merchant": "Store A", "amount": 20, "time": "2019-02-13T10:03:30.000Z"}
    assert validators.double_transaction(operation, delta_seconds=120) == []


def test_double_transaction_same_store_sequential_in_interval(monkeypatch):
    monkeypatch.setattr(validators.store, 'transactions', deque([
        {"merchant": "Store B", "amount": 20, "time": parser.parse("2019-02-13T10:01:00.000Z")},
        {"merchant": "Store A", "amount": 20, "time": parser.parse("2019-02-13T10:00:00.000Z")},
    ]))
    operation = {"merchant": "Store B", "amount": 20, "time": "2019-02-13T10:01:30.000Z"}
    violations = validators.double_transaction(operation, delta_seconds=120)
    assert violations == [validators.DOUBLED_TRANSACTION]


def test_double_transaction_same_store_sequential_in_interval_limit(monkeypatch):
    monkeypatch.setattr(validators.store, 'transactions', deque([
        {"merchant": "Store B", "amount": 20, "time": parser.parse("2019-02-13T10:01:00.000Z")},
        {"merchant": "Store A", "amount": 20, "time": parser.parse("2019-02-13T10:00:00.000Z")},
    ]))
    operation = {"merchant": "Store B", "amount": 20, "time": "2019-02-13T10:02:00.000Z"}
    violations = validators.double_transaction(operation, delta_seconds=120)
    assert violations == [validators.DOUBLED_TRANSACTION]


def test_double_transaction_same_store_no_sequential_in_interval(monkeypatch):
    monkeypatch.setattr(validators.store, 'transactions', deque([
        {"merchant": "Store B", "amount": 20, "time": parser.parse("2019-02-13T10:01:00.000Z")},
        {"merchant": "Store A", "amount": 20, "time": parser.parse("2019-02-13T10:00:00.000Z")},
    ]))
    operation = {"merchant": "Store A", "amount": 20, "time": "2019-02-13T10:01:30.000Z"}
    violations = validators.double_transaction(operation, delta_seconds=120)
    assert violations == [validators.DOUBLED_TRANSACTION]


def test_no_double_transaction_same_store_sequential_different_amount_in_interval(monkeypatch):
    monkeypatch.setattr(validators.store, 'transactions', deque([
        {"merchant": "Store B", "amount": 20, "time": parser.parse("2019-02-13T10:01:00.000Z")},
        {"merchant": "Store A", "amount": 20, "time": parser.parse("2019-02-13T10:00:00.000Z")},
    ]))
    operation = {"merchant": "Store B", "amount": 30, "time": "2019-02-13T10:01:30.000Z"}
    assert validators.double_transaction(operation) == []


def test_no_double_transaction_same_store_no_sequential_different_amount_in_interval(monkeypatch):
    monkeypatch.setattr(validators.store, 'transactions', deque([
        {"merchant": "Store B", "amount": 20, "time": parser.parse("2019-02-13T10:01:00.000Z")},
        {"merchant": "Store A", "amount": 20, "time": parser.parse("2019-02-13T10:00:00.000Z")},
    ]))
    operation = {"merchant": "Store A", "amount": 30, "time": "2019-02-13T10:01:30.000Z"}
    assert validators.double_transaction(operation) == []


def test_valid_operation():
    operation_obj = {
        'transaction': {"merchant": "Store A", "amount": 30, "time": "2019-02-13T10:01:30.000Z"}
    }
    assert validators.valid_operation(operation_obj) == []


def test_no_valid_operation():
    operation_obj = {
        'credit': {"merchant": "Store A", "amount": 30, "time": "2019-02-13T10:01:30.000Z"}
    }
    assert validators.valid_operation(operation_obj) == [validators.NO_VALID_OPERATION]


def test_no_exist_account_transaction(monkeypatch):
    monkeypatch.setattr(validators.store, 'account', {})
    assert validators.account_exist_transaction() == [validators.ACCOUNT_NOT_INITIALIZED]


def test_exist_account_transaction(monkeypatch):
    monkeypatch.setattr(validators.store, 'account', {"activeCard": True, "availableLimit": 100})
    assert validators.account_exist_transaction() == []