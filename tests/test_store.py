from collections import deque
from dateutil import parser

from authorizer import store


def test_get_account():
    store_new = store.Store()
    store_new._clean()
    account = {"activeCard": True, "availableLimit": 100}
    store_new.account = account
    assert store_new.get_account() == account
    assert store_new.get_account_obj() == {'account': account}


def test_set_account():
    store_new = store.Store()
    store_new._clean()
    account = {"activeCard": True, "availableLimit": 100}
    store_new.set_account(account)
    assert store_new.account == account


def test_get_transactions():
    store_new = store.Store()
    store_new._clean()
    transaction_a = {"merchant": "Store A", "amount": 20, "time": "2019-02-13T10:01:00.000Z"}
    transaction_b = {"merchant": "Store B", "amount": 30, "time": "2019-02-13T10:00:00.000Z"}
    transaction_a_parse = {"merchant": "Store A", "amount": 20, "time": parser.parse("2019-02-13T10:01:00.000Z")}
    transaction_b_parse = {"merchant": "Store B", "amount": 30, "time": parser.parse("2019-02-13T10:00:00.000Z")}
    transactions = deque([
        transaction_b_parse,
        transaction_a_parse,
    ])
    store_new.transactions = transactions
    assert store_new.get_transactions() == [transaction_b_parse, transaction_a_parse]
    assert store_new.get_transactions_obj() == [{'transaction': transaction_b}, {'transaction': transaction_a}]


def test_add_transaction():
    store_new = store.Store()
    store_new._clean()
    transaction = {"merchant": "Store A", "amount": 20, "time": "2019-02-13T10:01:00.000Z"}
    store_new._add_transaction(transaction)
    expected = deque([{"merchant": "Store A", "amount": 20, "time": parser.parse("2019-02-13T10:01:00.000Z")}])
    assert store_new.transactions == expected


def test_add_transactions_order():
    store_new = store.Store()
    store_new._clean()
    transaction_a = {"merchant": "Store A", "amount": 20, "time": "2019-02-13T10:01:00.000Z"}
    transaction_b = {"merchant": "Store B", "amount": 30, "time": "2019-02-13T10:01:05.000Z"}
    transaction_c = {"merchant": "Store C", "amount": 10, "time": "2019-02-13T10:01:30.000Z"}
    store_new._add_transaction(transaction_a)
    store_new._add_transaction(transaction_b)
    store_new._add_transaction(transaction_c)
    expected = deque([
        {"merchant": "Store C", "amount": 10, "time": parser.parse("2019-02-13T10:01:30.000Z")},
        {"merchant": "Store B", "amount": 30, "time": parser.parse("2019-02-13T10:01:05.000Z")},
        {"merchant": "Store A", "amount": 20, "time": parser.parse("2019-02-13T10:01:00.000Z")},
    ])
    assert store_new.transactions == expected


def test_apply_transaction():
    store_new = store.Store()
    store_new._clean()
    store_new.account = {"activeCard": True, "availableLimit": 100}
    transaction = {"merchant": "Store A", "amount": 20, "time": "2019-02-13T10:01:00.000Z"}
    store_new.apply_transaction(transaction)
    expected_transactions = deque([{"merchant": "Store A", "amount": 20, "time": parser.parse("2019-02-13T10:01:00.000Z")}])
    expected_account = {"activeCard": True, "availableLimit": 80}
    assert store_new.account == expected_account
    assert store_new.transactions == expected_transactions