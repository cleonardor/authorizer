from unittest import mock
from collections import deque

from authorizer import (
    brokers,
    clients,
)


def test_integration_std_client(capsys):
    brokers.store._clean()
    operations = [
        '{"account": {"activeCard": true, "availableLimit": 250}}',
        '{"transaction": {"merchant": "Store C2", "amount": 30, "time": "2019-02-13T10:01:30.000Z"}}',
        '{"account": {"activeCard": true, "availableLimit": 300}}',
        '{"transaction": {"merchant": "Store C2", "amount": 30, "time": "2019-02-13T10:02:30.000Z"}}',
        '{"transaction": {"merchant": "Store S2", "amount": 50, "time": "2019-02-13T10:04:30.000Z"}}',
        '{"transaction": {"merchant": "Store A2", "amount": 40, "time": "2019-02-13T10:04:50.000Z"}}',
        '{"transaction": {"merchant": "Store B2", "amount": 70, "time": "2019-02-13T10:05:30.000Z"}}',
        '{"transaction": {"merchant": "Store X2", "amount": 50, "time": "2019-02-13T10:05:57.000Z"}}',
        '{"transaction": {"merchant": "Store H2", "amount": 10, "time": "2019-02-13T10:08:00.000Z"}}',
    ]
    expecteds = [
        '{"account": {"activeCard": true, "availableLimit": 250}, "violations": []}\n',
        '{"account": {"activeCard": true, "availableLimit": 220}, "violations": []}\n',
        '{"account": {"activeCard": true, "availableLimit": 220}, "violations": ["account-already-initialized"]}\n',
        '{"account": {"activeCard": true, "availableLimit": 220}, "violations": ["doubled-transaction"]}\n',
        '{"account": {"activeCard": true, "availableLimit": 170}, "violations": []}\n',
        '{"account": {"activeCard": true, "availableLimit": 130}, "violations": []}\n',
        '{"account": {"activeCard": true, "availableLimit": 60}, "violations": []}\n',
        '{"account": {"activeCard": true, "availableLimit": 60}, "violations": ["high-frequency-small-interval"]}\n',
        '{"account": {"activeCard": true, "availableLimit": 50}, "violations": []}\n',
    ]
    def mock_input():
        return operations.pop(0)
    clients.input = mock_input
    output = []
    for expected in expecteds:
        clients.std_client()
        out, err = capsys.readouterr()
        output.append(out)
    assert output == expecteds


def test_integration_dispacher2(capsys):
    brokers.store._clean()
    operations = [
        '{"account": {"activeCard": false, "availableLimit": 250}}',
        '{"transaction": {"merchant": "Store C", "amount": 30, "time": "2019-02-13T10:01:30.000Z"}}',
        '{"account": {"activeCard": true, "availableLimit": 300}}',
        '{"transaction": {"merchant": "Store C", "amount": 30, "time": "2019-02-13T10:02:30.000Z"}}',
        '{"transaction": {"merchant": "Store S", "amount": 50, "time": "2019-02-13T10:04:30.000Z"}}',
        '{"transaction": {"merchant": "Store A", "amount": 40, "time": "2019-02-13T10:04:50.000Z"}}',
        '{"transaction": {"merchant": "Store B", "amount": 70, "time": "2019-02-13T10:05:30.000Z"}}',
        '{"transaction": {"merchant": "Store X", "amount": 50, "time": "2019-02-13T10:05:57.000Z"}}',
        '{"transaction": {"merchant": "Store H", "amount": 10, "time": "2019-02-13T10:08:00.000Z"}}',
    ]
    expecteds = [
        '{"account": {"activeCard": false, "availableLimit": 250}, "violations": []}\n',
        '{"account": {"activeCard": false, "availableLimit": 250}, "violations": ["card-not-active"]}\n',
        '{"account": {"activeCard": false, "availableLimit": 250}, "violations": ["account-already-initialized"]}\n',
        '{"account": {"activeCard": false, "availableLimit": 250}, "violations": ["card-not-active"]}\n',
        '{"account": {"activeCard": false, "availableLimit": 250}, "violations": ["card-not-active"]}\n',
        '{"account": {"activeCard": false, "availableLimit": 250}, "violations": ["card-not-active"]}\n',
        '{"account": {"activeCard": false, "availableLimit": 250}, "violations": ["card-not-active"]}\n',
        '{"account": {"activeCard": false, "availableLimit": 250}, "violations": ["card-not-active"]}\n',
        '{"account": {"activeCard": false, "availableLimit": 250}, "violations": ["card-not-active"]}\n',
    ]
    def mock_input():
        return operations.pop(0)
    clients.input = mock_input
    output = []
    for expected in expecteds:
        clients.std_client()
        out, err = capsys.readouterr()
        output.append(out)
    assert output == expecteds